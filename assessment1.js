//M2-Assessment 1
const {readFileSync, promises: fsPromises} = require('fs');

// a)
function aboutMe(name,institution, repository){
    console.log(`My name is  ${name} , a student from ${institution} and my Github username is ${repository}`)
}


aboutMe("Mkhululi","University of Johannesburg", "kay")

// b)
function predictWinner(categories){
    const winner = (categories[Math.floor(Math.random()*categories.length)]);
    console.log(winner)
    return winner
}
const categories = ["Best Consumer Solution", "Best Enterprise Solution", "Most Innovative Solution", "Best Gaming Solution","Best Health Solution","Best Agricultural Solution","Best Educational Solution", "Best Financial Solution", "Best Hackathon Solution", "Best South African Solution", "Best Campus Cup Solution", "Best African Solution"]

predictWinner(categories)

// c)
function viewFile(filename){
    const contents = readFileSync(filename, 'utf-8');
    console.log(contents);
    return contents
}

viewFile("winner.txt")